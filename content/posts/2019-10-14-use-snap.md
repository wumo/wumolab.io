+++
title= "Software Management: Scoop On Windows and Snap On Ubuntu"
date= "2019-10-14"
tags= ["snap","snapcraft","ubuntu","scoop","windows"]
+++
Use scoop to install apps on windows.
Use snap to install apps on ubuntu.

<!--more-->

## Installation
On Windows:
```powershell
$ Set-ExecutionPolicy RemoteSigned -scope CurrentUser
$ iwr -useb get.scoop.sh | iex
```
On Ubuntu:
```shell

# setting proxy
$ sudo snap set system proxy.http="http://<proxy_addr>:<proxy_port>"
$ sudo snap set system proxy.https="http://<proxy_addr>:<proxy_port>"
```

## Customize Installation

Scoop allows you to setup your own buckets to install your favourite softwares in your own way.

Steps [<u>details</u>](https://github.com/lukesampson/scoop/wiki/Creating-an-app-manifest):

1. Create a public git project for your buckets, like [<u>Scoop Main buckets</u>](https://github.com/ScoopInstaller/Main/tree/master/bucket).

2. Write the recipe for installing your software, like[<u>7zip.json</u>](https://github.com/ScoopInstaller/Main/blob/master/bucket/7zip.json)

3. Call `scoop bucket add <your-bucket-name> <your-bucket-git-url>` to add your bucket.

4. Install your software as you always do.

Samples:

* my-youtube-dl.json: create my favourite configuration for youtube-dl
```json
{
    "version": "1.0",
    "url": "https://gitlab.com/snippets/1901452/raw#/youtube-dl.conf",
    "hash": "ed77fe6dee67ade2e22194ef0127456e3a76e32c5bfbd73d41f5bd81a18ffc74",
    "depends": "youtube-dl",
    "post_install": [
        "$loc = \"~\\youtube-dl.conf\"",
        "New-Item -ItemType File -Path $loc -Force",
        "Copy-Item \"$dir\\youtube-dl.conf\" $loc -Force"
    ]
}
```

* my-alacritty.json: default configuration for alacritty
```json
{
    "version": "1.0",
    "depends": "alacritty",
    "url": [
        "https://gitlab.com/snippets/1901468/raw#/alacritty.yml",
        "https://gitlab.com/snippets/1901480/raw#/alacritty-install-context.reg",
        "https://gitlab.com/snippets/1901481/raw#/alacritty-uninstall-context.reg"
    ],
    "hash":[
        "d8a39e45d0cf1846acc5f002fe3ce1bf9a0a966a6d2b1012f8fe7cb0b65e6a5d",
        "fa3838d46a8b8e790998963acc81ccf8eed77dd4a88e0f913c1593d7d009d3fb",
        "5752f64c5a6f22062e30ce4da0a73f4bc28c93192a8445d3dc5e3c01b0198215"
    ],
    "post_install": [
        "$loc = \"$env:APPDATA\\alacritty\\alacritty.yml\"",
        "New-Item -ItemType File -Path $loc -Force",
        "Copy-Item \"$dir\\alacritty.yml\" $loc -Force",
        "$exepath = \"$scoopdir\\apps\\alacritty\\current\\alacritty.exe\".Replace('\\', '\\\\')",
        "$content = Get-Content \"$dir\\alacritty-install-context.reg\"",
        "$content = $content.Replace('$exe_path', $exepath)",
        "$content | Set-Content -Path \"$dir\\alacritty-install-context.reg\""
    ],
    "notes": "Add Alacritty as a context menu option by running: \"$dir\\alacritty-install-context.reg\""
}
```

* my-msys2.json: use [<u>tuna mirror</u>](https://mirror.tuna.tsinghua.edu.cn/) to update msys2 and install gcc automatically.
```json
{
    "homepage": "http://msys2.github.io",
    "description": "A software distro and building platform for Windows.",
    "##": "64-bit version is able to build both 32-bit and 64-bit packages",
    "version": "20190524",
    "license": "GPL-2.0-only|BSD-3-Clause",
    "architecture": {
        "64bit": {
            "url": "https://mirrors.tuna.tsinghua.edu.cn/msys2/distrib/x86_64/msys2-base-x86_64-20190524.tar.xz",
            "extract_dir": "msys64",
            "hash": "sha1:cfe5035b1b81b43469d16bfc23be8006b9a44455"
        },
        "32bit": {
            "url": "https://mirrors.tuna.tsinghua.edu.cn/msys2/distrib/i686/msys2-base-i686-20190524.tar.xz",
            "extract_dir": "msys32",
            "hash": "sha1:ff86c3e4ef8777074fd394510b95943d0c943956"
        }
    },
    "pre_install": [
        "if ($architecture -eq '32bit') {",
        "   $manifest.bin += ,@('autorebase.bat', 'autorebase')",
        "}"
    ],
    "post_install": [
        "Write-Output \"Configuring to use tuna mirror https://mirrors.tuna.tsinghua.edu.cn/msys2 ...\"",
        "$file = \"$dir\\etc\\pacman.d\\mirrorlist.mingw32\"",
        "$content = Get-Content -raw $file",
        "$content = \"Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/mingw/i686`r`n\"+$content",
        "$content | Set-Content -Path $file",
        "$file = \"$dir\\etc\\pacman.d\\mirrorlist.mingw64\"",
        "$content = Get-Content -raw $file",
        "$content = \"Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/mingw/x86_64`r`n\"+$content",
        "$content | Set-Content -Path $file",
        "$file = \"$dir\\etc\\pacman.d\\mirrorlist.msys\"",
        "$content = Get-Content -raw $file",
        "$content = \"Server = https://mirrors.tuna.tsinghua.edu.cn/msys2/msys/`$arch`r`n\"+$content",
        "$content | Set-Content -Path $file",
        "Write-Output \"Update msys2 packages and core system ...\"",
        "&\"$dir\\usr\\bin\\bash\" -l -c \"pacman -Syuu --noconfirm\"",
        "&\"$dir\\usr\\bin\\bash\" -l -c \"pacman -Syuu --noconfirm\"",
        "Write-Output \"Install latest gcc ...\"",
        "$arch = If ($architecture -eq '32bit') {\"i686\"} Else {\"x86_64\"}",
        "$toolchain = \"mingw-w64-{0}-toolchain\" -f $arch",
        "&\"$dir\\usr\\bin\\bash\" -l -c \"pacman -S $toolchain --noconfirm\""
    ],
    "bin": [
        [
            "msys2_shell.cmd",
            "msys2",
            "-msys2 -defterm -here -no-start"
        ],
        [
            "msys2_shell.cmd",
            "mingw",
            "-mingw -defterm -here -full-path -no-start"
        ],
        [
            "msys2_shell.cmd",
            "mingw32",
            "-mingw32 -defterm -here -full-path -no-start"
        ],
        [
            "msys2_shell.cmd",
            "mingw64",
            "-mingw64 -defterm -here -full-path -no-start"
        ]
    ],
    "shortcuts": [
        [
            "msys2.exe",
            "MSYS2"
        ],
        [
            "mingw32.exe",
            "MinGW32"
        ],
        [
            "mingw64.exe",
            "MinGW64"
        ]
    ],
    "persist": "home",
    "notes": "Please run 'msys2' now for the MSYS2 setup to complete!"
}

```