+++
title= "精彩的Concurrent、Lock-free、Memory Order博客memo"
date= "2017-05-17"
tags= ["concurrent","lock-free","memory order"]
+++
<!--more-->
 
# [JSR166 Concurrency](http://jsr166-concurrency.10961.n7.nabble.com/) forum 
* [Doug Lea's posts](http://jsr166-concurrency.10961.n7.nabble.com/template/NamlServlet.jtp?macro=user_nodes&user=23)
* [Using JDK 9 Memory Order Modes](http://gee.cs.oswego.edu/dl/html/j9mm.html)

# [Psychosomatic, Lobotomy, Saw](http://psy-lob-saw.blogspot.hk/) from Nitsan Wakart 
* [Java Concurrent Counters By Numbers](http://psy-lob-saw.blogspot.hk/2013/06/java-concurrent-counters-by-numbers.html)
* [What do Atomic*::lazySet/Atomic*FieldUpdater::lazySet/Unsafe::putOrdered* actually mean?](http://psy-lob-saw.blogspot.hk/2016/12/what-is-lazyset-putordered.html)
* [GC 'Nepotism' And Linked Queues](http://psy-lob-saw.blogspot.hk/2016/03/gc-nepotism-and-linked-queues.html)
* [Degrees Of (Lock/Wait) Freedom](http://psy-lob-saw.blogspot.hk/2015/05/degrees-of-lockwait-freedom.html)
* [Porting Pitfalls: Turning D.Vyukov MPSC Wait-free queue into a j.u.Queue](http://psy-lob-saw.blogspot.hk/2015/04/porting-dvyukov-mpsc.html)

# [1024cores](http://www.1024cores.net/) from Dmitry Vyukov
* [Memory model: Atomicity](http://www.1024cores.net/home/lock-free-algorithms/so-what-is-a-memory-model-and-how-to-cook-it)
* [Memory model: Visibility](http://www.1024cores.net/home/lock-free-algorithms/so-what-is-a-memory-model-and-how-to-cook-it/visibility)
* [Memory model: Ordering](http://www.1024cores.net/home/lock-free-algorithms/so-what-is-a-memory-model-and-how-to-cook-it/ordering)
* [Compiler vs. Hardware](http://www.1024cores.net/home/lock-free-algorithms/so-what-is-a-memory-model-and-how-to-cook-it/compiler-vs-hardware)
* [Scalability Prerequisites](http://www.1024cores.net/home/lock-free-algorithms/scalability-prerequisites)
* [Task Scheduling Strategies](http://www.1024cores.net/home/scalable-architecture/task-scheduling-strategies)
 
# [Preshing on Programming](http://preshing.com/) from Jeff Preshing
* [An Introduction to Lock-Free Programming](http://preshing.com/20120612/an-introduction-to-lock-free-programming/)
* [Acquire and Release Semantics](http://preshing.com/20120913/acquire-and-release-semantics/)
* [Memory Barriers Are Like Source Control Operations](http://preshing.com/20120710/memory-barriers-are-like-source-control-operations/)
* [This Is Why They Call It a Weakly-Ordered CPU](http://preshing.com/20121019/this-is-why-they-call-it-a-weakly-ordered-cpu/)


# [Mechanical Sympathy](https://mechanical-sympathy.blogspot.hk/) from Martin Thompson
* [CPU Cache Flushing Fallacy](https://mechanical-sympathy.blogspot.hk/2013/02/cpu-cache-flushing-fallacy.html)
* [Printing Generated Assembly Code From The Hotspot JIT Compiler](https://mechanical-sympathy.blogspot.hk/2013/06/printing-generated-assembly-code-from.html)
* [Java Garbage Collection Distilled](https://mechanical-sympathy.blogspot.hk/2013/07/java-garbage-collection-distilled.html)
* [Applying Back Pressure When Overloaded](https://mechanical-sympathy.blogspot.hk/2012/05/apply-back-pressure-when-overloaded.html)

# Atomic weapon from Herb Sutter
* [atomic Weapons: The C++ Memory Model and Modern Hardware](https://herbsutter.com/2013/02/11/atomic-weapons-the-c-memory-model-and-modern-hardware/)