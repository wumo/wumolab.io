+++
title= "Clion Miscellaneous"
date= "2019-10-15"
tags= ["clion"]
+++
Clion Miscellaneous Things:<!--more-->

# clion cannot find declaration to go to
Help menu > Edit Custom Properties
```shell
# Maximum file size (kilobytes)
idea.max.intellisense.filesize=2500
```