+++
title= "Launch4j Gradle"
date= "2019-09-30"
tags= ["launch4j","gradle"]
+++
Launch4j Gradle Configuration.

<!--more-->

[Launch4j](https://github.com/mirror/launch4j) wrap a java jar into an exe.
Add the following code to `build.gradle.kts` to enable `launch4j`:
```kotlin
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  application
  kotlin("jvm") version "1.3.50"
  id("com.github.johnrengelman.shadow") version "5.1.0"
  id("edu.sc.seis.launch4j") version "2.4.6"
}

group = "wumo"
version = "0.0.1"

repositories {
  mavenCentral()
}

dependencies {
  implementation(kotlin("stdlib-jdk8"))
  testImplementation("org.junit.jupiter:junit-jupiter:5.+")
  tasks.test {
    useJUnitPlatform()
  }
}

tasks.withType<KotlinCompile> {
  kotlinOptions.jvmTarget = "1.8"
}

application {
  mainClassName = "MyAppKt"
}

launch4j {
  val shadowJar by tasks.getting(ShadowJar::class)
  
  copyConfigurable = shadowJar.outputs.files
  jar = "lib/${shadowJar.archiveFileName.get()}"
  jreMinVersion = "1.8.0"
  headerType = "console" // or "gui"
  chdir = ""
}
```

**Note**: 

1. `launch4j` searches in `registry` for `HKEY_LOCAL_MACHINE\SOFTWARE\JavaSoft\JRE\1.XX@JavaHome` or `HKEY_LOCAL_MACHINE\SOFTWARE\JavaSoft\JDK\1.XX@JavaHome` to determine the java executable path. Manually add this registry if you don't have one.   
2. `headerType` should change to `gui` if you are developing a gui app.   
3. `chdir=""` ensures the working directory won't be changed if you launch the app from different folder. `chdir="."`will change the working directory to the path of your app.