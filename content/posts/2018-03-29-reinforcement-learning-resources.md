+++
title= "Reinforcement Learning Resources"
date= "2018-03-29"
tags= ["reinforcement learning"]
+++

<!--more-->
[Reinforcement Learning: An Introduction 2nd](http://incompleteideas.net/book/the-book-2nd.html)

[Matrix Differentiation](https://atmos.washington.edu/~dennis/MatrixCalculus.pdf)

[Understanding Machine Learning: From Theory to Algorithms](https://www.cambridge.org/core/books/understanding-machine-learning/3059695661405D25673058E43C8BE2A6)